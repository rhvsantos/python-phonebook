from sql_script import INIT_DB
from contextlib import closing
from functools import total_ordering
import time
import sys
import sqlite3
import os
import os.path


class ErrorDeletePhone(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class IDNotFound(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class NewPhoneError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class EditContactNameError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class PhonetypeError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def isEmptyOrNull(value):
    return value is None or not value.strip()


class Person:
    def __init__(self, name, _id=None):
        self.name = name
        self.id = _id

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, person_name):
        if person_name.strip():
            self.__name = person_name
        else:
            raise ValueError('Error: person_name must be a String!')

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, person_id):
        if person_id is None:
            self.__id = None

        elif isinstance(person_id, int):
            self.__id = int(person_id)
        else:
            raise ValueError('Error: person_id must be an Integer or None')

    def __str__(self):
        return self.name

    def __eq__(self, o):
        if isinstance(o, str):
            compare = Person('o')
        elif not isinstance(o, Person):
            raise TypeError('To compare, the another object must be a string or Person object.')
        else:
            compare = o

        return self.name.lower() == compare.name.lower()

    def __lt__(self, o):
        if not isinstance(o, Person):
            raise TypeError('It must be an instance of Person Class.')
        return self.name < o.name

    def __repr__(self):
        return f'<Class: {type(self).__name__} on 0x{id(self):x} Name: {self.__name} id: {self.__id}>'


@total_ordering
class PhoneType:
    def __init__(self, description, type_id=None):
        self.description = description
        self.id = type_id

    def __str__(self):
        return f'{self.description}'

    def __eq__(self, o):
        if not isinstance(o, PhoneType):
            o = PhoneType(o)
        return self.description.lower() == o.description.lower()

    def __lt__(self, o):
        if isinstance(o, PhoneType):
            return self.description < o.description
        else:
            raise TypeError('It must be an instance of PhoneType Class.')

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id_value):

        if id_value is None:
            self.__id = None
        elif isinstance(id_value, str):
            if id_value.isdigit():
                self.__id = int(id_value)
        elif isinstance(id_value, int):
            self.__id = id_value
        else:
            raise TypeError('The given argument did not meet the requirements. (str number, int or None).')


class PhoneTypes:
    def __init__(self):
        self.type_list = []

    def search_type(self, phonetype):
        if not isinstance(phonetype, PhoneType):
            raise TypeError('It must be an instance of PhoneType Class.')

        if phonetype in self.type_list:
            obj_index = self.type_list.index(phonetype)
            return self.type_list[obj_index]
        else:
            return None

    def new_phonetype(self, phonetype):
        if self.search_type(phonetype) is None:
            self.type_list.append(phonetype)
            print(f'[+] Phonetype {phonetype.description} was loaded.')
        else:
            print('This phonetype already exists.')
            time.sleep(1)

    def sort_types(self):
        self.type_list.sort()

    def __len__(self):
        return len(self.type_list)

    def __iter__(self):
        return iter(self.type_list)

    def __getitem__(self, index):
        return self.type_list[index]


class Phone:
    def __init__(self, phonenumber, phonetype=None, _id=None):
        self.phonenumber = phonenumber
        self.phonetype = phonetype
        self.id = _id

    def __repr__(self):
        return f'Class: {type(self).__name__} on 0x{id(self):x} phonenumber: {self.__phonenumber} phonetype: {self.__phonetype.description} id: {self.__id}'

    def __str__(self):
        return f'{self.__phonenumber} {self.__phonetype.description}'

    def __eq__(self, o):
        if isinstance(o, str):
            o = Phone(o)

        if isinstance(o, Phone):
            return self.phonenumber == o.phonenumber
        else:
            raise TypeError('To compare, the another object must be a string or a Phone object.')

    @property
    def phonetype(self):
        return self.__phonetype

    @phonetype.setter
    def phonetype(self, phonetype):
        if phonetype is None:
            self.__phonetype = None
        elif isinstance(phonetype, PhoneType):
            self.__phonetype = phonetype
        else:
            raise TypeError(f'PhoneType is optional, but if you want to add it, must be an instance of PhoneType Class.')

    @property
    def phonenumber(self):
        return self.__phonenumber

    @phonenumber.setter
    def phonenumber(self, number):
        self.__phonenumber = number

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id_value):

        if id_value is None:
            self.__id = None
        elif isinstance(id_value, str):
            if id_value.isdigit():
                self.__id = int(id_value)
        elif isinstance(id_value, int):
            self.__id = id_value
        else:
            raise TypeError('The given argument did not meet the requirements. (str number, int or None).')


class PhoneList:
    def __init__(self):
        self.phones = []

    def __iter__(self):
        return iter(self.phones)

    def __getitem__(self, element):
        return self.phones[element]

    def __len__(self):
        return len(self.phones)

    def new_phone(self, phone_obj):
        if not isinstance(phone_obj, Phone):
            raise TypeError('It must be an instance of Phone Class.')

        if not phone_obj in self.phones:
            self.phones.append(phone_obj)

    def del_phone(self, phone_obj):
        if not isinstance(phone_obj, Phone):
            raise TypeError('It must be an instance of Phone Class.')

        if phone_obj in self.phones:
            self.phones.remove(phone_obj)

    def search_phone(self, phonenumber):
        if isinstance(phonenumber, str):
            phonenumber_obj = Phone(phonenumber)
        else:
            phonenumber_obj = phonenumber

        for phone in self.phones:
            if phonenumber_obj == phone:
                return phone
        else:
            return None


@total_ordering
class PersonPhone:
    def __init__(self, person, _id=None):
        self.person = person
        self.phones = PhoneList()
        self.id = _id

    def __len__(self):
        return len(self.phones)

    def __str__(self):
        return self.__person.name

    def __lt__(self, o):
        if not isinstance(o, PersonPhone):
            raise TypeError('It must be an instance of PersonPhone Class.')
        return self.person < o.person

    def __repr__(self) -> str:
        return f'<Class {type(self).__name__} on 0x{id(self):x} Person: {self.__person.name}'

    def __iter__(self):
        return iter(self.phones)

    def __getitem__(self, element):
        return self.phones[element]

    def __eq__(self, o):
        if isinstance(o, str):
            obj = PersonPhone(Person(o))
        elif isinstance(o, Person):
            obj = PersonPhone(o)
        elif isinstance(o, PersonPhone):
            obj = o
        else:
            raise TypeError('It must be a str or an instance of Person or PersonPhone Class.')
        return obj.person.name.lower().strip() == self.person.name.lower().strip()

    @property
    def person(self):
        return self.__person

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, personphone_id):
        if personphone_id is None:
            self.__id = None

        elif isinstance(personphone_id, str):
            if personphone_id.isdigit():
                self.__id = int(personphone_id)
            else:
                raise ValueError('PersonPhoneID Must be an Integer!')
        elif isinstance(personphone_id, int):
            self.__id = personphone_id
        else:
            raise ValueError('Error: PersonPhoneID did not meet requirements.')

    @person.setter
    def person(self, person_obj):
        if not isinstance(person_obj, Person):
            raise TypeError('It must be an instance of Person Class.')
        self.__person = person_obj


class Phonebook:
    def __init__(self, db_file):
        self.data = []
        self.phonetypes = PhoneTypes()
        new = not os.path.isfile(db_file)

        self.connection = sqlite3.connect(db_file)
        self.connection.row_factory = sqlite3.Row

        if new:
            self.connection.executescript(INIT_DB)
            print('[OK] Database was created!')
        else:
            print('[OK] Database already exist, initializing it!')

    def __iter__(self):
        return iter(self.data)

    def __repr__(self):
        return f'<Class {type(self).__name__} on 0x{id(self):x}>'

    def __getitem__(self, index):
        return self.data[index]

    def __len__(self):
        return len(self.data)

    def sort_phonebook(self):
        self.data.sort(key=lambda person: person.person.name.lower())

    def load_contacts(self):
        '''Load all contacts from DB to self.data.'''
        with closing(self.connection.cursor()) as cursor:
            all_names = cursor.execute('SELECT id, name FROM names').fetchall()

            for _id, _name in all_names:
                # Create a Person instance
                person_instance = Person(_name, _id)

                # Create an empty PersonPhone instance (without phonenumber)
                personphone_instance = PersonPhone(person_instance, _id)

                # Search for all phonenumbers of the current person
                person_phones = cursor.execute(
                    'SELECT id, phonenumber, phonetype_id FROM phones WHERE person_name_id = ?', (_id,)).fetchall()

                # Append all person phonenumbers into PersonPhone object
                self.data.append(self.personphone_build(personphone_instance, person_phones))

        # Sort the phonebook
        self.sort_phonebook()

        print(f'[OK] All contacts have been loaded from DB. ({len(self)})')
        time.sleep(1)

    def add_newperson(self, person) -> str:
        if not isinstance(person, PersonPhone):
            raise TypeError('It must be an instance of PersonPhone Class.')

        # Ensure that our Phonebook doesn't has a contact with the same name (Avoiding duplicity)
        with closing(self.connection.cursor()) as cursor:
            query = cursor.execute('SELECT COUNT(*) FROM names WHERE name = ?', (person.person.name,)).fetchone()

            # If the Query didn't find any contact with the same name
            if query[0] == 0:
                try:
                    # Insert the new contact's name into names table
                    cursor.execute('INSERT INTO names(name) VALUES(?)', (person.person.name,))

                    # Store the person's ID into Person instance
                    person.person.id = cursor.lastrowid

                    # Set the same ID for PersonPhone instance
                    person.id = cursor.lastrowid
                    return person.person.id
                except Exception:
                    # If something went wrong, do a rollback and raise the error
                    self.connection.rollback()
                    raise
                finally:
                    cursor.close()
            else:
                print('This contact name already exists.')
                return None

    def add_person_phoneDB(self, phone, person_id) -> None:
        '''It gonna check of the argument is a valid PersonPhone object, if it is the case, it will seach for duplicity. In case of duplicity, it will print a message telling this
        . Or if it is a uniq DB entry, it will include the Person name on names table and all person phonenumbers on phonenumbers table. It will include into the instance, the
        ID property.'''
        # Ensure person argument is a Phone instance.
        if not isinstance(phone, Phone):
            raise TypeError('It must be an instance of Phone Class.')

        with closing(self.connection.cursor()) as cursor:
            try:
                # Save the new entry in the database (phones table)
                cursor.execute('INSERT INTO phones(phonenumber, phonetype_id, person_name_id) VALUES(?, ?, ?)',
                               (phone.phonenumber, phone.phonetype.id, person_id))
                # Commit all changes
                self.connection.commit()
                # Set the Phone ID
                phone.id = cursor.lastrowid

                if phone.id is None:
                    raise NewPhoneError('Could not find Phone ID.')
            except Exception:
                # If something went wrong, do a rollback and raise the error
                self.connection.rollback()
                raise
            finally:
                cursor.close()

    def del_person_phoneDB(self, phone):

        # Ensure person argument is a Phone instance.
        if not isinstance(phone, Phone):
            raise TypeError('It must be an instance of Phone Class.')

        try:
            self.connection.execute('DELETE FROM phones WHERE id = ?', (phone.id,))
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            raise
        else:
            return True

    def update_person_phoneDB(self, phone) -> Phone:

        if not isinstance(phone, Phone):
            raise TypeError('It must be an instance of Phone Class.')

        if phone.id is None:
            raise IDNotFound('This Phone instance has no ID to track.')

        with closing(self.connection.cursor()) as cursor:
            try:
                cursor.execute('UPDATE phones SET phonenumber = ?, phonetype_id = ? WHERE id = ?',
                               (phone.phonenumber, phone.phonetype.id, phone.id))
                self.connection.commit()
            except Exception:
                self.connection.rollback()
                raise
            else:
                return True

    def del_contact(self, person) -> None:
        '''It only takes the PersonPhone name property from the instance, if it was found, it will delete it from DB'''

        if not isinstance(person, PersonPhone):
            raise TypeError('It must be an instance of PersonPhone Class.')

        cursor = self.connection.cursor()
        query = cursor.execute('SELECT COUNT(*) FROM names WHERE name = ?', (person.person.name,)).fetchone()

        if query[0] == 0:
            print(f'{person.person.name} was not found.')
            cursor.close()
            return

        try:
            # Delete name using its ID
            cursor.execute('DELETE FROM names WHERE id = ?', (person.person.id,))

            # Delete each phonenumber of this person
            cursor.execute('DELETE FROM phones WHERE person_name_id = ?', (person.person.id,))

            # Commit the changes
            self.connection.commit()

            # Set PersonPhone ID to None
            person.person.id = None

            # Delete it from self.data
            self.data.remove(person)

        except:
            # Rollback the changes if something went wrong.
            self.connection.rollback()
            raise
        finally:
            # Close the db cursor
            cursor.close()

    def update_person(self, person):

        if not isinstance(person, PersonPhone):
            raise TypeError('It must be an instance of PersonPhone Class')

        try:
            self.connection.execute('UPDATE names SET name = ? WHERE id = ?', (person.person.name, person.person.id))
            self.connection.commit()
        except Exception:
            self.connection.rollback()
            raise
        else:
            return True

    def search(self, person) -> PersonPhone:
        '''Search for the given PersonPhone object (looking for the name property) and retrieve all person's data from DB and if found.
        Returning it attaching as a PersonPhone instance. If not found, it will return None.'''

        if not isinstance(person, PersonPhone):
            raise TypeError('It must be an instance of PersonPhone Class')

        if person in self.data:
            index = self.data.index(person)
            return self.data[index]
        else:
            return None

    def personphone_build(self, person, phones) -> PersonPhone:
        '''For each person's phonenumbers, create a new Phone Instance and append it on PersonPhone instance.'''

        if person is None:
            return None
        elif person.person.id is None:
            return None

        for phone_id, phone, type_id in phones:
            for phonetype in self.phonetypes:
                if type_id == phonetype.id:
                    person.phones.new_phone(Phone(phone, phonetype, phone_id))
                    break
        return person

    def new_phonetype(self, description) -> None:
        '''You will add phonetypes to the self.phonetypes and DB using this Method'''

        if not self.phonetypes.search_type(PhoneType(description)):
            cursor = self.connection.cursor()

            try:
                cursor.execute('INSERT INTO phonetypes(description) VALUES(?)', (description,))
                new_phonetype_id = cursor.lastrowid
                self.connection.commit()
            except Exception:
                self.connection.rollback()
                raise
            else:
                self.phonetypes.new_phonetype(PhoneType(description, new_phonetype_id))
                print('Done!')
                time.sleep(1)
            finally:
                cursor.close()
        else:
            print('This Phonetype already exists.')
            time.sleep(1)

    def load_phonetypes(self) -> None:
        '''All phonetypes stored into phonetypes Table will be loaded to memory (self.phonetypes).'''
        with closing(self.connection.cursor()) as cursor:

            cursor.execute('SELECT id, description from phonetypes')

            while True:
                phonetype = cursor.fetchone()

                if phonetype is None:
                    break

                self.phonetypes.new_phonetype(PhoneType(phonetype['description'], phonetype['id']))

        print('[OK] Phonetypes were loaded from DB.')


class Menu:
    '''This method will receive a list of option like this:
    [['Option Name', 'Function'], ['Another name', 'Function'] and so on...'''

    def __init__(self, option_list):
        self.options = option_list
        self.options.insert(0, ['Quit', 'break'])

    def check_option(self, opt):
        if opt.isdigit():
            if 0 <= int(opt) < len(self.options):
                return True
        return False

    def show_options(self):
        for ind, opt in enumerate(self.options):
            print(f'{[ind]} - {opt[0]}')

    def execute(self) -> str:
        '''This method will show the Menu (using show_options method), check the selected option (usint check_option) and will return the selected option.'''
        while True:
            self.show_options()
            print('-' * 60)
            choice = input('Select an option above: ')
            if self.check_option(choice):
                return int(choice)


class PhonebookApp:

    @staticmethod
    def show_data(person, just_one=True):
        print(f'\nName: {person.person.name:<20} ID: {person.person.id}')

        if len(person) > 0:
            for ind, phone in enumerate(person.phones):
                print(f' [{ind}] - {phone}')
        else:
            print('\t[This contact has no phonenumber registered yet.]\n')
        if just_one:
            input()

    @staticmethod
    def has_phone(person):
        if len(person) == 0:
            return False
        else:
            return True

    @staticmethod
    def ask_name():
        name = input('Name: ').strip()

        if not isEmptyOrNull(name):
            return name
        else:
            return None

    @staticmethod
    def ask_phonenumber():
        phonenumber = input('Phonenumber: ').strip()

        if not isEmptyOrNull(phonenumber):
            if PhonebookApp.check_phonenumber(phonenumber):
                return phonenumber

        return None

    @staticmethod
    def check_phonenumber(phonenumber):

        if phonenumber.replace(' ', '').replace('-', '').isdigit():
            return True
        else:
            print('Phonenumber syntax is wrong.')
            return False

    @staticmethod
    def select_phonetype(phonetype_list):
        while True:
            for ind, phonetype in enumerate(phonetype_list):
                print(f'[{ind}] - {phonetype.description}')
            choice = input('Select a Phonetype: ').strip()

            if not isEmptyOrNull(choice):
                try:
                    choice = int(choice)
                except ValueError:
                    raise PhonetypeError("Wrong phonetype")

                if choice in range(len(phonetype_list)):
                    return phonetype_list[choice]

    def __init__(self, database):
        self.phonebook = Phonebook(database)
        self.phonebook.load_phonetypes()
        self.phonebook.load_contacts()
        self.main_options = [['Show all Contacts', self.show_contacts], ['New Contact', self.new_contact], [
            'Remove Contact', self.del_contact], ['Edit Contact', self.edit_contact]]
        self.main_menu = Menu(self.main_options)

    def new_contact(self):
        cls()
        print('*' * 60)
        print('New Contact'.center(60))
        print('*' * 60)
        name = PhonebookApp.ask_name()

        if name is None:
            return

        new_instance = PersonPhone(Person(name))

        if new_instance in self.phonebook.data:
            print('You already have this contact into your phonebook.')
            time.sleep(1)
            return

        person_id = self.phonebook.add_newperson(new_instance)

        if person_id is not None:

            new_instance.person.id = person_id
            new_instance.id = person_id

            self.show_phone_menu(new_instance)

            if len(new_instance) == 0:
                print(f'You did not include any phonenumber to {name}, you can change it later.')

            # Insert the new contact into self.phonebook.data
            self.phonebook.data.append(new_instance)

            print(f'Now {new_instance.person.name} is in your Phonebook!')
            time.sleep(1)
        else:
            print('Error: Something went wrong. Try later :)')

    def show_phone_menu(self, person):
        while True:
            cls()

            phone_options = [['New Phonenumber', self.new_phone], ['Remove Phonenumber', self.del_phone], [
                'Edit Phonenumber', self.edit_phone], ['Edit Contact Name', self.edit_contact_name], ['Show Phonenumbers', PhonebookApp.show_data]]

            print('*' * 60)
            if not PhonebookApp.has_phone(person):
                del phone_options[1:]
                print(f'New contact {person.person.name}'.center(60))
            else:
                print(f'Editing {person.person.name}'.center(60))

            phone_menu = Menu(phone_options)

            print('*' * 60)
            choice = phone_menu.execute()
            if choice == 0:
                print('Returning...')
                time.sleep(1)
                break
            phone_options[choice][1](person)

    def del_contact(self):
        cls()
        if self.isempty():
            return

        print('*' * 60)
        print('Remove Contact'.center(60))
        print('*' * 60)
        name = PhonebookApp.ask_name()

        if name is None:
            return

        new_instance = self.phonebook.search(PersonPhone(Person(name)))

        if new_instance is None:
            print(f'{name} was not found!')
            time.sleep(1)
            return

        self.phonebook.del_contact(new_instance)
        print('Done!')
        time.sleep(1)

    def edit_contact(self):
        cls()
        if self.isempty():
            return

        print('*' * 60)
        print('Edit Contact'.center(60))
        print('*' * 60)
        name = PhonebookApp.ask_name()

        if name is None:
            return

        _get_person = self.phonebook.search(PersonPhone(Person(name)))

        if _get_person is None:
            print('Not found!')
            time.sleep(1)
            return

        self.show_phone_menu(_get_person)

        # Update person on self.phonebook.data list
        self.phonebook.data.remove(_get_person)
        self.phonebook.data.append(_get_person)

    def edit_contact_name(self, person):
        cur_name = person.person.name

        new_name = PhonebookApp.ask_name()

        if new_name is None or new_name == cur_name:
            print('No changes.')
            time.sleep(1)
            return
        else:
            person.person.name = new_name

            if person.person.id is not None:
                if not self.phonebook.update_person(person):
                    raise EditContactNameError('Error while trying to change the contact name.')

            print('New name has been set.')
            time.sleep(1)
            return

    def del_phone(self, person):
        if not PhonebookApp.has_phone(person):
            print('This contact has no registered phonenumber yet.')
            time.sleep(1)
            return

        PhonebookApp.show_data(person)
        _delete = input('Which phonenumber do you want do Delete? ')

        if _delete.isdigit():
            _delete = int(_delete)
            if 0 <= _delete < len(person.phones):
                _selected_phone = person.phones[_delete]
                _ask = input(f'''-> {_selected_phone}
Are you sure do you want to delete the following Phonenumber? (y/n) ''').lower()

                if _ask == 'y':

                    if person.person.id is not None:
                        if not self.phonebook.del_person_phoneDB(_selected_phone):
                            raise ErrorDeletePhone(f'Could not delete {_selected_phone.phonenumber}')

                    person.phones.del_phone(_selected_phone)
                    print('Done!')
                    time.sleep(1)
                    return

        print('Aborted!')

    def edit_phone(self, person):
        if not PhonebookApp.has_phone(person):
            print('This contact has no registered phonenumber yet.')
            time.sleep(1)
            return

        PhonebookApp.show_data(person)
        _edit = input('Which phonenumber do you want do Edit? ')

        if _edit.isdigit():
            _edit = int(_edit)
            if 0 <= _edit < len(person.phones):
                _selected_phone = person.phones[_edit]

        new_number = PhonebookApp.ask_phonenumber()

        if new_number is not None:
            _selected_phone.phonenumber = new_number

        phonetype = PhonebookApp.select_phonetype(self.phonebook.phonetypes)
        _selected_phone.phonetype = phonetype

        if not self.phonebook.update_person_phoneDB(_selected_phone):
            raise Exception('Error while trying to edit the contact.')

        print('Done!')
        time.sleep(1)

    def new_phone(self, person):
        phone = PhonebookApp.ask_phonenumber()
        if phone is None:
            return

        if person.phones.search_phone(phone) is None:
            try:
                phonetype = PhonebookApp.select_phonetype(self.phonebook.phonetypes)
            except Exception:
                return
            new_phone_instance = Phone(phone, phonetype)

            self.phonebook.add_person_phoneDB(new_phone_instance, person.person.id)
            person.phones.new_phone(new_phone_instance)
            print('Done!')
            time.sleep(1)

        else:
            print('This phonenumber is already registered.')
            time.sleep(1)
            return

    def isempty(self):
        if len(self.phonebook) == 0:
            cls()
            print('Your phonebook is empty!')
            time.sleep(1)
            return True

    def show_contacts(self):
        if self.isempty():
            return

        # Sort phonebook
        self.phonebook.sort_phonebook()

        for contact in self.phonebook:
            PhonebookApp.show_data(contact, just_one=False)
        input()

    def show_main_menu(self):
        while True:
            cls()
            print('*' * 60)
            print('Phonebook'.center(60))
            print('*' * 60)
            choice = self.main_menu.execute()
            if choice == 0:
                print('Quitting...')
                time.sleep(1)
                break
            self.main_options[choice][1]()


if __name__ == "__main__":
    if len(sys.argv) == 2:
        app = PhonebookApp(sys.argv[1])
        app.show_main_menu()
    else:
        print(f'''
        Usage:
            {sys.argv[0]} <Database file>
        ''')
