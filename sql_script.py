INIT_DB = '''
CREATE TABLE names(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL);
CREATE TABLE phonetypes(id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT NOT NULL);
CREATE TABLE phones(id INTEGER PRIMARY KEY AUTOINCREMENT, phonenumber TEXT NOT NULL, phonetype_id INT NOT NULL, person_name_id INT NOT NULL);
INSERT INTO phonetypes(description) VALUES('Cellphone');
INSERT INTO phonetypes(description) VALUES('Home');
INSERT INTO phonetypes(description) VALUES('Work');
INSERT INTO phonetypes(description) VALUES('Fax');
'''