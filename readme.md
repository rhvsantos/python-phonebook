# MyPhonebook

<p>I've made this Phonebook App as part of my Python studies. Here I could practice and understand more about Object-Oriented Programming. I did this by myself, but inspired by a Python book called <b>Introdução à programação com Python</b>, by Nilo Ney Coutinho Menezes, that I just read.</p>

## About

<p>Like any Phonebook App, with this App you can store all your contacts and their phonenumber, and all user's data will be stored into a  sqlite3 database.</p>

### How to use it

> $ python MyPhonebook.py \<sqlite3_db_file.db\>

* On the firt run, the application will create the entire database schema. <p>** (Important: The given db file should not exist.)</p>

After running the above command, an app's menu will be shown to you, where you can create your contacts and manage the entire phonebook in an intuitive way. :)
